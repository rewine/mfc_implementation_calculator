﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 IntCalculator.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_INTCALCULATOR_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_EDIT_VALUE                  1000
#define IDC_B0                          1001
#define IDC_B1                          1002
#define IDC_B2                          1003
#define IDC_B3                          1004
#define IDC_B4                          1005
#define IDC_B5                          1006
#define IDC_B6                          1007
#define IDC_B7                          1008
#define IDC_B8                          1009
#define IDC_B9                          1010
#define IDC_BADD                        1011
#define IDC_BSUB                        1012
#define IDC_BMUL                        1013
#define IDC_BDIV                        1014
#define IDC_BEQUAL                      1015
#define IDC_BC                          1016
#define IDC_BDot                        1017
#define IDC_BLEFT                       1018
#define IDC_BRIGTH                      1019
#define IDC_BDEC                        1020
#define IDC_CHECK_RADIO                 1022
#define IDC_CHECK1                      1022
#define IDC_RADIO_DEC                   1024
#define IDC_RADIO_OCT                   1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
