﻿
// IntCalculatorDlg.h: 头文件
//

#pragma once


// CIntCalculatorDlg 对话框
class CIntCalculatorDlg : public CDialogEx
{
// 构造
public:
	CIntCalculatorDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_INTCALCULATOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedB6();
	CString m_sEquation;
	afx_msg void OnClickedB0();
	afx_msg void OnClickedB1();
	afx_msg void OnClickedB2();
	afx_msg void OnClickedB3();
	afx_msg void OnClickedB4();
	afx_msg void OnClickedB5();
	afx_msg void OnClickedB7();
	afx_msg void OnClickedB8();
	afx_msg void OnClickedB9();
	afx_msg void OnClickedBadd();
	afx_msg void OnClickedBc();
	afx_msg void OnClickedBdiv();
	afx_msg void OnClickedBequal();
	afx_msg void OnClickedBmul();
	afx_msg void OnClickedBsub();
	afx_msg void OnBnClickedBdot();
	afx_msg void OnBnClickedBleft();
	afx_msg void OnBnClickedBrigth();
	afx_msg void OnBnClickedBdec();
//	afx_msg void OnClickedRadioBin();
	afx_msg void OnBnClickedRadioOct();
	afx_msg void OnBnClickedRadioDec();
	int m_radix;
	afx_msg void OnBnClickedCheck1();
};
