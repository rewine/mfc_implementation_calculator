﻿
// IntCalculatorDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "IntCalculator.h"
#include "IntCalculatorDlg.h"
#include "afxdialogex.h"
#include "MyCalculator.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CIntCalculatorDlg 对话框



CIntCalculatorDlg::CIntCalculatorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_INTCALCULATOR_DIALOG, pParent)
	, m_sEquation(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_radix = 10;
}

void CIntCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_VALUE, m_sEquation);
}

BEGIN_MESSAGE_MAP(CIntCalculatorDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_B6, &CIntCalculatorDlg::OnBnClickedB6)
	ON_BN_CLICKED(IDC_B0, &CIntCalculatorDlg::OnClickedB0)
	ON_BN_CLICKED(IDC_B1, &CIntCalculatorDlg::OnClickedB1)
	ON_BN_CLICKED(IDC_B2, &CIntCalculatorDlg::OnClickedB2)
	ON_BN_CLICKED(IDC_B3, &CIntCalculatorDlg::OnClickedB3)
	ON_BN_CLICKED(IDC_B4, &CIntCalculatorDlg::OnClickedB4)
	ON_BN_CLICKED(IDC_B5, &CIntCalculatorDlg::OnClickedB5)
	ON_BN_CLICKED(IDC_B7, &CIntCalculatorDlg::OnClickedB7)
	ON_BN_CLICKED(IDC_B8, &CIntCalculatorDlg::OnClickedB8)
	ON_BN_CLICKED(IDC_B9, &CIntCalculatorDlg::OnClickedB9)
	ON_BN_CLICKED(IDC_BADD, &CIntCalculatorDlg::OnClickedBadd)
	ON_BN_CLICKED(IDC_BC, &CIntCalculatorDlg::OnClickedBc)
	ON_BN_CLICKED(IDC_BDIV, &CIntCalculatorDlg::OnClickedBdiv)
	ON_BN_CLICKED(IDC_BEQUAL, &CIntCalculatorDlg::OnClickedBequal)
	ON_BN_CLICKED(IDC_BMUL, &CIntCalculatorDlg::OnClickedBmul)
	ON_BN_CLICKED(IDC_BSUB, &CIntCalculatorDlg::OnClickedBsub)
	ON_BN_CLICKED(IDC_BDot, &CIntCalculatorDlg::OnBnClickedBdot)
	ON_BN_CLICKED(IDC_BLEFT, &CIntCalculatorDlg::OnBnClickedBleft)
	ON_BN_CLICKED(IDC_BRIGTH, &CIntCalculatorDlg::OnBnClickedBrigth)
	ON_BN_CLICKED(IDC_BDEC, &CIntCalculatorDlg::OnBnClickedBdec)
//	ON_BN_CLICKED(IDC_RADIO_BIN, &CIntCalculatorDlg::OnClickedRadioBin)
ON_BN_CLICKED(IDC_RADIO_OCT, &CIntCalculatorDlg::OnBnClickedRadioOct)
ON_BN_CLICKED(IDC_RADIO_DEC, &CIntCalculatorDlg::OnBnClickedRadioDec)
ON_BN_CLICKED(IDC_CHECK1, &CIntCalculatorDlg::OnBnClickedCheck1)
END_MESSAGE_MAP()


// CIntCalculatorDlg 消息处理程序

BOOL CIntCalculatorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	((CButton*)GetDlgItem(IDC_CHECK1))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_RADIO_DEC))->SetCheck(1);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CIntCalculatorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CIntCalculatorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CIntCalculatorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CIntCalculatorDlg::OnBnClickedB6()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '6';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB0()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '0';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '1';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB2()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '2';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB3()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '3';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB4()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '4';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB5()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '5';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB7()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '7';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB8()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '8';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedB9()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '9';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBadd()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '+';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBc()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation = "";
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBdiv()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '/';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBequal()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
//	m_sEquation += '=';
	CString ans;
	if (!MyCalculator::Calc(m_sEquation, ans, m_radix))
		AfxMessageBox(_T("请检查表达式！"));
	else
		m_sEquation = ans;
//	AfxMessageBox(debeg);
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBmul()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '*';
	UpdateData(false);
}


void CIntCalculatorDlg::OnClickedBsub()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '-';
	UpdateData(false);
}


void CIntCalculatorDlg::OnBnClickedBdot()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '.';
	UpdateData(false);
}


void CIntCalculatorDlg::OnBnClickedBleft()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += '(';
	UpdateData(false);
}


void CIntCalculatorDlg::OnBnClickedBrigth()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	m_sEquation += ')';
	UpdateData(false);
}


void CIntCalculatorDlg::OnBnClickedBdec()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	int T = m_sEquation.GetLength();
	if (T > 0)
		m_sEquation = m_sEquation.Left(T-1);
	UpdateData(false);
}


void CIntCalculatorDlg::OnBnClickedRadioOct()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	CButton* m_B8 = (CButton*)this->GetDlgItem(IDC_B8);
	m_B8->EnableWindow(FALSE);
	CButton* m_B9 = (CButton*)this->GetDlgItem(IDC_B9);
	m_B9->EnableWindow(FALSE);
	if (!m_sEquation.IsEmpty()) {
		CString stmp;
		bool cas = MyCalculator::Calc(m_sEquation, stmp, m_radix);
		if (cas) {
			bool cas2 = MyCalculator::ConvertRad(stmp, m_radix, 8);
			if (cas2) m_sEquation = stmp;
			else m_sEquation = "";
			//	m_sEquation = stmp;
		}
		else {
			m_sEquation = "";
		}
	}
	m_radix = 8;
	UpdateData(FALSE);
}


void CIntCalculatorDlg::OnBnClickedRadioDec()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	CButton* m_B8 = (CButton*)this->GetDlgItem(IDC_B8);
	m_B8->EnableWindow(TRUE);
	CButton* m_B9 = (CButton*)this->GetDlgItem(IDC_B9);
	m_B9->EnableWindow(TRUE);
	if (!m_sEquation.IsEmpty()) {
		CString stmp;
		bool cas = MyCalculator::Calc(m_sEquation, stmp, m_radix);
		if (cas) {
			bool cas2 = MyCalculator::ConvertRad(stmp, m_radix, 10);
			if (cas2) m_sEquation = stmp;
			else m_sEquation = "";
		//	m_sEquation = stmp;
		}
		else {
			m_sEquation = "";
		}
	}
	m_radix = 10;
	UpdateData(FALSE);
}


void CIntCalculatorDlg::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	CButton* p = (CButton*)this->GetDlgItem(IDC_CHECK1);
	CButton* r1 = (CButton*)this->GetDlgItem(IDC_RADIO_DEC);
	CButton* r2 = (CButton*)this->GetDlgItem(IDC_RADIO_OCT);
	if (p->GetCheck() == BST_CHECKED) {
		r1->EnableWindow(TRUE);
		r2->EnableWindow(TRUE);
	}
	else {
		r1->EnableWindow(FALSE);
		r2->EnableWindow(FALSE);
	}
}
